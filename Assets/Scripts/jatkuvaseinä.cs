﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jatkuvaseinä : MonoBehaviour
{

    public GameObject pelaaja;

    private BoxCollider2D groundCollider;
    public float seinänkorkeus;

    public float kesto;

    // Start is called before the first frame update
    void Start()
    {
        groundCollider = GetComponent<BoxCollider2D>();
        seinänkorkeus = 24f;
    
    }

    // Update is called once per frame
    void Update()
    {
       if (transform.position.y - kesto > pelaaja.transform.position.y)
        {         
            RepositionBackground();
        }
    }

    private void RepositionBackground()
    {
        Vector2 matka = new Vector2(transform.position.x, seinänkorkeus);
        transform.position = new Vector2(transform.position.x, transform.position.y - seinänkorkeus);
     
    }
}