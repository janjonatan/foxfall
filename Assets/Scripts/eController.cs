﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class eController : MonoBehaviour
{
    
    private Rigidbody2D rbe;
    private Animator eanim;
    private SpriteRenderer sp;
    private Rigidbody2D rb;
    private AudioSource eaudios;

    private bool ealive = true;
    public AudioClip ded;

    private BoxCollider2D coll;

    
    void Start()
    {
        eanim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        eaudios = GetComponent<AudioSource>();
        coll = GetComponent<BoxCollider2D>();
    }



    public IEnumerator Destroy()
    {
        yield return new WaitForSeconds(0.8f);
        Destroy(this.gameObject);       
    }

    void OnCollisionEnter2D(Collision2D osu)
    {
        if (osu.gameObject.tag == "Player" && ealive == true)
        {
            eanim.SetTrigger("edead");
            Destroy(coll);
            ealive = false;
            eaudios.PlayOneShot(ded, 0.3f);
            StartCoroutine(Destroy());
        }
    }


}












    

