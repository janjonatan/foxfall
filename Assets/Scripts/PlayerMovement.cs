﻿using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


public class PlayerMovement : MonoBehaviour
{


    //testi
    [SerializeField]
    private float jumpforce = 400f, runSpeed = 16, walkSpeed = 8,maxVel = 22;

    private float fallSpeed;

    
    public Text points;
    public Text time;

    float horizontalMove = 0f;
    bool falling = false;
    bool alive = true;

    private static int tapot;
    private static float aika;
    

    //raycastaus
    public float dis = 0.8f;
    private Vector2 dir = -Vector2.up;

    private Vector3 velocity = Vector3.zero;

    private Rigidbody2D rb;
    protected Animator anim;
    private SpriteRenderer sp;


    //adioshiit
    private AudioSource audios;
    public AudioClip audiorun;
    public AudioClip audiojump;
    public AudioClip dead;
    

    void Start()
    {
        audios = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sp = GetComponent<SpriteRenderer>();

        points.text = "0";

        PlayerPrefs.SetInt("tapot", tapot);
        Pisteet();      
    }

   
    public IEnumerator Respawn()
    {
        audios.PlayOneShot(dead, 0.4f);
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);      
    }


    void Pisteet()
    {
        points.text = tapot.ToString();        
        time.text = "Your time: " + aika.ToString();     
    }
    
    
    void OnCollisionEnter2D(Collision2D osu)
    {
        if (SceneManager.GetActiveScene().buildIndex == 0)
        {
            if (osu.gameObject.tag == "Enemy")
            {             
                rb.AddForce(new Vector2(0, 400));

                tapot = 0;
                PlayerPrefs.SetInt("tapot", tapot);
            }          
        }

       else if (osu.gameObject.tag == "Enemy")
        {           
            rb.AddForce(new Vector2(0, 900));
            tapot += 1;
            Pisteet();
        }

        if (osu.gameObject.tag == "Teleporter")
        {
            aika = Time.timeSinceLevelLoad;
            PlayerPrefs.SetFloat("aika", aika);
            aika = Mathf.Round(aika * 10.0f) * 0.1f;
            Pisteet();
        }
    }

    void Kuolema()
        {
    alive = false;
            anim.SetTrigger("Dead");
            rb.velocity = new Vector2(0, 0);
    StartCoroutine(Respawn());
        tapot = 0;
}

    

    void Update()
    {

        int GMask = 1 << 8;
        int SMask = 1 << 10;
      //  int EMask = 1 << 11;
        
        

        //Tehdään raycast jolla on alkamiskohta, suunta ja matka     
            RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, dis, GMask); //hyppyyn
            RaycastHit2D hit2 = Physics2D.Raycast(transform.position, new Vector2(-0.4f, -1), dis, SMask); //piikkeihin  
            RaycastHit2D hit22 = Physics2D.Raycast(transform.position, new Vector2(0.4f, -1), dis, SMask); //piikkeihin2

         //   RaycastHit2D hit3 = Physics2D.Raycast(transform.position, dir, dis - 0.3f, EMask); //vihollsiin
        

        //Tekee testiviivan raycastauskesta
        Debug.DrawRay(transform.position, new Vector2(0, -dis), Color.cyan);
        Debug.DrawRay(transform.position, new Vector2(-0.4f, -dis), Color.red);
        Debug.DrawRay(transform.position, new Vector2(0.4f, -dis), Color.red);

        // Ottaa X aksiksen arvon ja kertoo sen laitetulla juoksunopeudella

        horizontalMove = Input.GetAxisRaw("Horizontal") * fallSpeed;
        Vector3 targetVel = new Vector2 (horizontalMove, rb.velocity.y);


        // Tarkastaa onko pelaaja elossa jotta voi liikkua
        if (alive == true)
        {
            rb.velocity = Vector3.SmoothDamp(rb.velocity, targetVel, ref velocity, 0);
        }

   

            // Tarkistaa onko hahmon y negatiivinen eli putoaako hahmo
            if (rb.velocity.y < -0.1)
        {
            falling = true;
        }
        else
        {
            falling = false;
        }

            
      /* if (hit3.collider != null && alive == true)
        {
            rb.velocity = Vector2.zero;
            rb.velocity = Vector2.zero;
            rb.velocity = Vector2.zero;

            rb.AddForce(new Vector2(0, 400));

        }  */

       

        // Jos hahmo osuu 2 raycastiin niin kuolema animaatio alkaa
        if (hit22.collider != null && alive == true)
        {
            Kuolema();        }

        // Jos hahmo osuu 2 raycastiin niin kuolema animaatio alkaa (kahdesti koska bugi)

        if (hit2.collider != null&& alive == true)
        {
            Kuolema();
        }

        //Hahmo hyppää
        if (Input.GetButtonDown("Jump") && hit.collider != null && alive == true)
        {           
            rb.AddForce(new Vector2(0f, jumpforce));

            if (Input.GetButtonDown("Jump") && anim.GetBool("Ground") == true)
            {
                audios.PlayOneShot(audiojump, 0.2f);
            }
        }

        //Hahmo on ilmassa ja animaatio on "putoaminen"
        if (hit.collider == null && falling == true && alive == true)
        {
            anim.SetBool("Ground", false);
            anim.SetTrigger("Down");
        }


        //Hahmo on ilmassa ja animaatio on "hyppy"
        if (hit.collider == null && falling == false && alive == true)
        {
            anim.SetBool("Ground", false);
            anim.SetTrigger("Up");      
        }
        else if (hit.collider != null && alive == true)
        {
            anim.SetBool("Ground", true);
        }


        //Hahmo liikkuminen
        if (rb.velocity.magnitude > 0 && hit.collider != null && alive == true && rb.velocity.y == 0f)
          {            
              anim.SetBool("Running", true);
            if (anim.GetBool("Running") == true && anim.GetBool("Ground") == true && !audios.isPlaying)
            {
                audios.PlayOneShot(audiorun, 0.2f);

            }


        }
          else if (rb.velocity.magnitude == 0)
          {         
            anim.SetBool("Running", false);        
        }

        
        //Vaihtaa hahmon spriten suuntaa jos x on negatiivinen ja vaihtaa sen takaisin jos positiivinen
        if (rb.velocity.x < 0)
        {
            sp.flipX = true;
        }
        else if (rb.velocity.x > 0)
        {
            sp.flipX = false;
        }

        if (Input.GetButtonDown("Cancel"))
        {
            SceneManager.LoadScene(0);
        }



        if (Input.GetButton("Vertical") == true)
        {
            fallSpeed = walkSpeed;
        }
        else
        {
            fallSpeed = runSpeed;
        }

    }

     void FixedUpdate()
    {


        // Maksimi tippuvuunopeus määrittä maxVel      
        if (rb.velocity.y < -maxVel)
        {
            rb.velocity = new Vector2(rb.velocity.x, -maxVel);
        }

       // rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxVel);
        
    }

}
