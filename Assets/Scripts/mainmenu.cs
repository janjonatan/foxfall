﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;



public class mainmenu : MonoBehaviour
{
    private AudioSource audios;
    public AudioClip startb;
    public AudioClip exitb;

    public string scene;
    public Color loadToColor = Color.white;

    private void Start()
    {
        audios = GetComponent<AudioSource>();
    }


    public void PlayGame()
    {

        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Initiate.Fade(scene, loadToColor, 2f);
        audios.PlayOneShot(startb, 0.8f);
    }


    public void QuitGame()

    {
        audios.PlayOneShot(exitb, 0.4f);
        Application.Quit();
    }

}