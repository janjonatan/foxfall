﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycasttest : MonoBehaviour
{

    public float dis = 0.8f;
    private Vector2 dir = -Vector2.up;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Cast a ray straight down.
        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, dis);

        if(hit.collider != null)
        {
            //If the object hit is less than or equal to 6 units away from this object.
            if (hit.distance <= 1.0f)
            {
                Debug.Log("Osuu");
            }
        }     


        Debug.DrawRay(transform.position, new Vector3(0,-dis,0), Color.cyan);
        

    }
}
